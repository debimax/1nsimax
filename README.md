# 1NSImax

Mes cours pour la classe de 1° NSI  2020-2021

- [1NSImax sur jupyterhub](https://megamaths.hd.free.fr/jupyter) 
- [1NSImax sur mybinder](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fdebimax%2F1nsimax/master)
- Mes anciens cours de 1° [2019-2020](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fdebimax%2Fcours-debimax/master)

